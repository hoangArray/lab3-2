// (C) 2013-2015, Sergei Zaychenko, KNURE, Kharkiv, Ukraine

#ifndef _ACCOUNT_HPP_
#define _ACCOUNT_HPP_
#include <iostream>
#include "messages.hpp"
/*****************************************************************************/
class Generator;
class Account
{
public:
    Account(std::string _clientName, double _currentBalance, int _accountID);
    
    virtual ~Account();

    int getAccountId();
    std::string const & getAccountName();
    double getBalance();
    virtual double getLimitOverdraft() { return 0; };
    
    void setBalance(double _amount);
    
    virtual bool isWithdrawalLimitExceeded(double _amount){ return false; };
    
private:
    
    int accountID;
    std::string clientName;
    double currentBalance;
};

inline void Account::setBalance(double _amount){
    currentBalance = _amount;
}
inline int Account::getAccountId(){
    return accountID;
}
inline std::string const & Account::getAccountName(){
    return clientName;
}
inline double Account::getBalance(){
    return currentBalance;
}



/*****************************************************************************/

#endif // _ACCOUNT_HPP_
