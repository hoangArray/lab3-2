//
//  GenerateAccountId.hpp
//  lab 3-2
//
//  Created by Anton Hoang on 16/03/2019.
//  Copyright © 2019 Anton Hoang. All rights reserved.
//

#ifndef GenerateAccountId_hpp
#define GenerateAccountId_hpp
#include <iostream>
#include <stdio.h>


class Generator{
    
public:
    int getGenerateRandomId();
};


#endif /* GenerateAccountId_hpp */
