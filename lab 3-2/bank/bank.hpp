// (C) 2013-2015, Sergei Zaychenko, KNURE, Kharkiv, Ukraine

#ifndef _BANK_HPP_
#define _BANK_HPP_
#include <iostream>
#include <vector>

/*****************************************************************************/

class Account;
class Bank
{

public:
    Bank();
    
    ~Bank();
    
    void setAccount(Account * _account);
    
    std::vector<Account *>const & getAccounts();
    
    Account * findAccountByID(int accountID) const;
    
private:
    std::vector<Account* > accounts;
};

inline void Bank::setAccount(Account * _account){
    accounts.push_back(_account);
}

inline std::vector<Account *>const & Bank::getAccounts(){
    return accounts;
}


/*****************************************************************************/


#endif // _BANK_HPP_
