// (C) 2013-2015, Sergei Zaychenko, KNURE, Kharkiv, Ukraine

#include "bank.hpp"
#include "account.hpp"

Bank::Bank(){
    
}
Bank::~Bank(){
    std::for_each(accounts.begin(), accounts.end(),
                  [&](Account * account) {
                      delete account;
                  });
    accounts.clear();
}

Account * Bank::findAccountByID(int accountID) const{
    Account * acc = nullptr;
    
    std::for_each(accounts.begin(), accounts.end(),
                  [&](Account * account){
                      if(account->getAccountId() == accountID){
                          acc = account;
                      }
                  });
    return acc;
//         throw std::logic_error(Messages::UnknownAccountID);
}

    
    


//    for(auto it = accounts.begin(); it != accounts.end(); it++){
//        if((*it)->getAccountId() == accountID){
//            return (*it);
//        }
//    }

