// (C) 2013-2015, Sergei Zaychenko, KNURE, Kharkiv, Ukraine

#ifndef _CONTROLLER_HPP_
#define _CONTROLLER_HPP_

/*****************************************************************************/

#include <string>
#include <vector>
#include <memory>
#include "bank.hpp"
/*****************************************************************************/
class Generator;
class OverdraftAccount;
class Account;
class Controller{

public:

	Controller ();

	Controller ( const Controller & ) = delete;
	
	Controller & operator = ( const Controller & ) = delete;

/*-----------------------------------------------------------------*/

	// @return ID of the created account
	int createAccount (
			std::string const & _ownerName
		,	double _initialBalance
	);

	// @return ID of the created account
	int createOverdraftAccount (
			std::string const & _ownerName
		,	double _initialBalance
		,	double _overdraftLimit
	);

	std::string const & getAccountOwnerName ( int _accountID ) const;

	double getAccountBalance ( int _accountID ) const;

	bool isOverdraftAllowed ( int _accountID ) const;

	double getOverdraftLimit ( int _accountID ) const;

	void deposit ( int _accountID, double _amount );

	void withdraw ( int _accountID, double _amount );

	void transfer ( int _sourceAccountID, int _targetAccountID, double _amount );

	double getTotalBankBalance () const;

	double getTotalBankDeposits () const;

	double getTotalBankOverdrafts () const;

/*-----------------------------------------------------------------*/

private:

    std::unique_ptr<Bank> bank;
    Generator * generatorID;
    bool isUniqueName(std::string const & _name);
    Account & getAccountByID(int _accountID) const;
};

#endif //  _CONTROLLER_HPP_
