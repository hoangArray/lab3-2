// (C) 2013-2015, Sergei Zaychenko, KNURE, Kharkiv, Ukraine

#include "controller.hpp"
#include "messages.hpp"
#include "account.hpp"
#include "bank.hpp"
#include "overdraftaccount.hpp"
#include "GenerateAccountId.hpp"
#include <algorithm>
#include <numeric>
/*****************************************************************************/

Account & Controller::getAccountByID(int _accountID) const{
    Account * acc = bank->findAccountByID(_accountID);
    if (acc == nullptr) {
        throw std::logic_error(Messages::UnknownAccountID);
    }
    return *acc;
}

bool Controller::isUniqueName(std::string const & _name){
    
    std::vector<Account *>const & accounts = bank->getAccounts();
    bool founded = false;
    std::for_each(accounts.begin(), accounts.end(),
                  [&](Account * const account) {
                      if (account->getAccountName() == _name) {
                          founded = true;
                      }
                  });
    return !founded;
}

Controller::Controller(){
    
    bank = std::make_unique<Bank>();
}

// @return ID of the created account
int Controller::createAccount (std::string const & _ownerName,
                                           double  _initialBalance){
    
    if(_ownerName.empty()){
        throw std::logic_error(Messages::OwnerNameIsEmpty);
    }
    if(_initialBalance < 0){
        throw std::logic_error(Messages::NegativeInitialBalance);
    }
    if(!isUniqueName(_ownerName)) {
        throw std::logic_error(Messages::OwnerNameNotUnique);
    }
    
    Account * account = new Account(_ownerName, _initialBalance, generatorID->getGenerateRandomId());
    bank->setAccount(account);
    return account->getAccountId();
}

// @return ID of the created account
int Controller::createOverdraftAccount (std::string const & _ownerName,
                                                    double  _initialBalance,
                                                    double  _overdraftLimit){
    
    if(_ownerName.empty()){
        throw std::logic_error(Messages::OwnerNameIsEmpty);
    }
    
    if(!isUniqueName(_ownerName)) {
        throw std::logic_error(Messages::OwnerNameNotUnique);
    }
    
    if(_initialBalance < 0){
        throw std::logic_error(Messages::NegativeInitialBalance);
    }
    
    if(_overdraftLimit < 0){
        throw std::logic_error(Messages::NegativeOverdraft);
    }
    
    OverdraftAccount * overdraft = new OverdraftAccount(_ownerName,
                                                        _initialBalance,
                                                        generatorID->getGenerateRandomId(),
                                                        _overdraftLimit);
    bank->setAccount(overdraft);
    return overdraft->getAccountId();
}

std::string const & Controller::getAccountOwnerName ( int _accountID ) const{
    
    Account & account = this->getAccountByID(_accountID);
    std::string const & name = account.getAccountName();
    return name;
    
}

double Controller::getAccountBalance ( int _accountID ) const{
    
    Account & account = this->getAccountByID(_accountID);

    return account.getBalance();
}

bool Controller::isOverdraftAllowed ( int _accountID ) const{
    
    Account & account = this->getAccountByID(_accountID);

    //Try to type conversition by dynamic_cast to Overdraft account if account is allowed return true else false
    OverdraftAccount const * overdraft = dynamic_cast <OverdraftAccount const*>(&account);
    return overdraft != nullptr;
}

double Controller::getOverdraftLimit ( int _accountID ) const{
    
    Account & account = this->getAccountByID(_accountID);
    return account.getLimitOverdraft();
}

void Controller::deposit ( int _accountID, double _amount ){
    
    if(_amount <= 0){
        throw std::logic_error(Messages::NonPositiveDeposit);
    }
    Account & account = this->getAccountByID(_accountID);

    account.setBalance(_amount + account.getBalance());
}

void Controller::withdraw ( int _accountID, double _amount ){
    
    Account & account = this->getAccountByID(_accountID);

    if(_amount <= 0){
        throw std::logic_error(Messages::NonPositiveWithdrawal);
    }
    if(_amount > account.getBalance()){
        if (!isOverdraftAllowed(_accountID)) {
            throw std::logic_error(Messages::WithdrawalLimitExceeded);
        }
        if (account.isWithdrawalLimitExceeded(_amount)) {
            throw std::logic_error(Messages::WithdrawalLimitExceeded);
        }
    }
    double newBalance = account.getBalance() - _amount;
    account.setBalance(newBalance);
}

void Controller::transfer ( int _sourceAccountID,
                            int _targetAccountID,
                            double _amount ){
    
    if(_amount <= 0){
        throw std::logic_error(Messages::NonPositiveTransfer);
    }
    Account & targetAccount = this->getAccountByID(_targetAccountID);

    withdraw(_sourceAccountID, _amount);
    targetAccount.setBalance(_amount  + targetAccount.getBalance());
}

double Controller::getTotalBankBalance () const{
    auto const & accounts = bank->getAccounts();
    int result = 0;
    std::for_each(accounts.begin(), accounts.end(),
                  [&](Account * const account) {
                      result += account->getBalance();
                  });
    return result;
}


double Controller::getTotalBankDeposits () const{
    auto const & accounts = bank->getAccounts();
    int result = 0;
    std::for_each(accounts.begin(), accounts.end(),
                  [&](Account * const account) {
                      if(account->getBalance() > 0 ) {
                          result += account->getBalance();
                      }
                  });
    return result;
}

double Controller::getTotalBankOverdrafts () const{
    
    auto const & accounts = bank->getAccounts();
    double overdraftsSum = 0;
    std::for_each(accounts.begin(), accounts.end(),
                  [&](Account * const account) {
                      if (account -> getBalance() < 0) {
                          overdraftsSum += account->getBalance() * (-1);
                      }
                  });
    return overdraftsSum;
}

/*****************************************************************************/
