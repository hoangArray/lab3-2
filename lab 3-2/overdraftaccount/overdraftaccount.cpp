// (C) 2013-2015, Sergei Zaychenko, KNURE, Kharkiv, Ukraine

#include "overdraftaccount.hpp"

/*****************************************************************************/
OverdraftAccount::OverdraftAccount(std::string _clientName,
                                        double _currentBalance,
                                        int    _accountID,
                                        double _limitOverdraft)
        :Account(_clientName, _currentBalance,_accountID),
  limitOverdraft(_limitOverdraft) {
      
}






/*****************************************************************************/
