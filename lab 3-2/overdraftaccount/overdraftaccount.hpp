// (C) 2013-2015, Sergei Zaychenko, KNURE, Kharkiv, Ukraine

#ifndef _OVERDRAFTACCOUNT_HPP_
#define _OVERDRAFTACCOUNT_HPP_

/*****************************************************************************/

#include "account.hpp"
#include "Messages.hpp"

/*****************************************************************************/


class OverdraftAccount:
        public Account
{
public:
    OverdraftAccount(std::string _clientName,
                          double _currentBalance,
                          int    _accountID,
                          double _limitOverdraft);
    
    double getLimitOverdraft() override;
    
private:
    
    double limitOverdraft;
    bool isWithdrawalLimitExceeded(double _amount) override;
};

inline  double OverdraftAccount::getLimitOverdraft(){
    return limitOverdraft;
}

inline bool OverdraftAccount::isWithdrawalLimitExceeded(double _amount){

    double sum = limitOverdraft + getBalance();
    return sum < _amount;
}

#endif // _OVERDRAFTACCOUNT_HPP_
